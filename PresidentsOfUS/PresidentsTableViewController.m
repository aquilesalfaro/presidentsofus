//
//  PresidentsTableViewController.m
//  PresidentsOfUS
//
//  Created by Aquiles Alfaro on 11/19/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "PresidentsTableViewController.h"


@interface PresidentsTableViewController ()<UISearchBarDelegate>
    
    @property (nonatomic, strong)NSDictionary* ourPresidents;
    @property (nonatomic, strong)NSArray* keys;
    @property (nonatomic) BOOL isFiltered;
@property (nonatomic, strong)NSMutableArray* filteredPresidents;
@property (weak, nonatomic) IBOutlet UISearchBar *searchPres;

@end

@implementation PresidentsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* path = [[NSBundle mainBundle]pathForResource:@"presidents" ofType:@"plist"];
    
    self.ourPresidents = [NSDictionary dictionaryWithContentsOfFile:path];
    
    self.keys = [[self.ourPresidents allKeys]sortedArrayUsingSelector:@selector(compare:)];

    self.isFiltered = false;
    self.searchPres.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search bar delegate methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0){
        self.isFiltered = false;
    }else{
        self.isFiltered = true;
        self.filteredPresidents = [[NSMutableArray alloc]init];
        
        for (NSString* pres in self.keys){
            NSRange nameRange = [pres rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound){
                [self.filteredPresidents addObject:pres];
            }
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isFiltered)
        return [self.filteredPresidents count];
    else
        return self.keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     PresTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"presCell" forIndexPath:indexPath];
    NSString* key;
    if(self.isFiltered){
        key = self.filteredPresidents[indexPath.section];
    }else{
        key = self.keys[indexPath.section];
    }
    //NSString *key = self.keys[indexPath.section];

    NSArray *keyValues = self.ourPresidents[key];
    NSLog(@"%@", keyValues);
    
    if (indexPath.section % 2 == 0){
        cell.backgroundColor = [UIColor lightGrayColor];
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
                          
    cell.lblPresName.text = keyValues[0];
    cell.lblPresParty.text = keyValues[8];
    cell.presImage.image = [UIImage imageNamed:keyValues[9]];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"presidentDetail"]){PresidentDetailViewController *detailVC = [segue destinationViewController];
        NSIndexPath* myIndexPath = [self.tableView indexPathForSelectedRow];
        //NSString* key = [[[self.tableView cellForRowAtIndexPath:myIndexPath]textLabel] text];
        
        president* aPres = [[president alloc]init];
        
        NSString* key = [[[self.tableView cellForRowAtIndexPath:myIndexPath] lblPresName] text] ;
       
        NSArray* keyValues = self.ourPresidents[key];
        
        aPres.name = keyValues[0];
        aPres.born = keyValues[1];
        aPres.bornIn = keyValues[2];
        aPres.died = keyValues[3];
        aPres.death = keyValues[4];
        aPres.tookOffice = keyValues[5];
        aPres.terms = keyValues[6];
        aPres.priorOccupation = keyValues[7];
        aPres.politicalParty = keyValues[8];
        aPres.image = keyValues[9];
        
        
        detailVC.myPresident = aPres;
        
    }
    
    
}


@end
