//
//  president.h
//  PresidentsOfUS
//
//  Created by Aquiles Alfaro on 11/19/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface president : NSObject
    @property(nonatomic, strong)NSString* name;
    @property(nonatomic, strong)NSString* born;
    @property(nonatomic, strong)NSString* bornIn;
    @property(nonatomic, strong)NSString* died;
    @property(nonatomic, strong)NSString* death;
    @property(nonatomic, strong)NSString* tookOffice;
    @property(nonatomic, strong)NSString* terms;
    @property(nonatomic, strong)NSString* priorOccupation;
    @property(nonatomic, strong)NSString* politicalParty;
    @property(nonatomic, strong)NSString* image;

@end
