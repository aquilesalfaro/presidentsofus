//
//  PresidentDetailViewController.m
//  PresidentsOfUS
//
//  Created by Aquiles Alfaro on 11/19/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "PresidentDetailViewController.h"

@interface PresidentDetailViewController ()


@end

@implementation PresidentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myPresName.text = self.myPresident.name;
    self.myPresBorn.text = self.myPresident.born;
    self.myPresDied.text = self.myPresident.died;
    self.myPresDiedAt.text = self.myPresident.death;
    self.myPresTook.text = self.myPresident.tookOffice;
    self.myPresParty.text = self.myPresident.politicalParty;
    self.myPresTerms.text = self.myPresident.terms;
    self.myPresBornIn.text = self.myPresident.bornIn;
    self.myPresPriorOcc.text = self.myPresident.priorOccupation;
    self.myPresImage.image = [UIImage imageNamed:self.myPresident.image];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
