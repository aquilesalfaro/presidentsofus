//
//  PresTableViewCell.h
//  PresidentsOfUS
//
//  Created by Aquiles Alfaro on 11/19/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *presImage;
@property (weak, nonatomic) IBOutlet UILabel *lblPresName;
@property (weak, nonatomic) IBOutlet UILabel *lblPresParty;

@end
