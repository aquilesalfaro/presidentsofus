//
//  PresidentDetailViewController.h
//  PresidentsOfUS
//
//  Created by Aquiles Alfaro on 11/19/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "president.h"

@interface PresidentDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *myPresName;
@property (weak, nonatomic) IBOutlet UILabel *myPresParty;
@property (weak, nonatomic) IBOutlet UILabel *myPresBorn;
@property (weak, nonatomic) IBOutlet UILabel *myPresBornIn;
@property (weak, nonatomic) IBOutlet UILabel *myPresDied;
@property (weak, nonatomic) IBOutlet UILabel *myPresDiedAt;
@property (weak, nonatomic) IBOutlet UILabel *myPresTook;
@property (weak, nonatomic) IBOutlet UILabel *myPresTerms;
@property (weak, nonatomic) IBOutlet UILabel *myPresPriorOcc;
@property (weak, nonatomic) IBOutlet UIImageView *myPresImage;

@property (strong, nonatomic)president* myPresident;


@end
