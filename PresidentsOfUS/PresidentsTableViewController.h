//
//  PresidentsTableViewController.h
//  PresidentsOfUS
//
//  Created by Aquiles Alfaro on 11/19/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "president.h"
#import "PresTableViewCell.h"
#import "PresidentDetailViewController.h"

@interface PresidentsTableViewController : UITableViewController
@property(nonatomic, strong)NSString* myPresName;
@property(nonatomic, strong)NSString* myPresBorn;
@property(nonatomic, strong)NSString* myPresBornIn;
@property(nonatomic, strong)NSString* myPresDied;
@property(nonatomic, strong)NSString* myPresDeath;
@property(nonatomic, strong)NSString* myPresTookOffice;
@property(nonatomic, strong)NSString* myPresTerms;
@property(nonatomic, strong)NSString* myPresOccupation;
@property(nonatomic, strong)NSString* myPresParty;
@property(nonatomic, strong)NSString* myPresImage;






@end
